package random

import (
	"golang.org/x/exp/rand"
	"time"
)

var charset = []byte("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567890")

func NewRandomString(length int) string {
	//	charSet := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567890")
	rnd := rand.New(rand.NewSource(uint64(time.Now().UnixNano())))

	b := make([]byte, length)

	for i := range b {
		b[i] = charset[rnd.Intn(len(charset))]
	}

	return string(b)
}
