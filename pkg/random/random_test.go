package random

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewRandomString(t *testing.T) {

	tests := []struct {
		name   string
		length int
		want   int
	}{
		{
			name:   "len 1",
			length: 1,
			want:   1,
		},

		{
			name:   "len 3",
			length: 3,
			want:   3,
		},

		{
			name:   "len 4",
			length: 4,
			want:   4,
		},

		{
			name:   "len 5",
			length: 5,
			want:   5,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			res1 := NewRandomString(tt.length)
			res2 := NewRandomString(tt.length)

			assert.NotEqual(t, res1, res2)
			assert.Equal(t, tt.want, len(res1))
			assert.Equal(t, tt.want, len(res2))
		})
	}
}
