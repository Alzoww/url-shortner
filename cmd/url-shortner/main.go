package main

import (
	"gitlab.com/Alzoww/url-shortner/internal/config"
	"gitlab.com/Alzoww/url-shortner/internal/http-server/handlers"
	"gitlab.com/Alzoww/url-shortner/pkg/logger/handlers/slogpretty"
	"golang.org/x/exp/slog"
	"net/http"
	"os"
)

func main() {
	cfg := config.MustLoadConfig()

	log := setupLogger(cfg.Env)
	log.Info("starting url-shortner logger...", slog.String("env", cfg.Env))
	log.Debug("debug messages are enabled")
	log.Error("error messages are enabled")

	r, err := handler.Router(log, cfg)
	if err != nil {
		log.Error("failed to initialize router", err)
		os.Exit(1)
	}

	log.Info("starting http server...", slog.String("address", cfg.Address))
	srv := &http.Server{
		Addr:         cfg.Address,
		Handler:      r,
		ReadTimeout:  cfg.HTTPServer.Timeout,
		WriteTimeout: cfg.HTTPServer.Timeout,
		IdleTimeout:  cfg.HTTPServer.IdleTimeout,
	}

	srv.ListenAndServe()
}

func setupLogger(env string) *slog.Logger {
	var log *slog.Logger

	switch env {
	case "local":
		log = setupPrettySlog()
	case "dev":
		log = slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}))
	case "prod":
		log = slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelInfo}))

	}

	return log
}

func setupPrettySlog() *slog.Logger {
	opts := slogpretty.PrettyHandlerOptions{
		SlogOpts: &slog.HandlerOptions{
			Level: slog.LevelDebug,
		},
	}

	handler := opts.NewPrettyHandler(os.Stdout)

	return slog.New(handler)
}
