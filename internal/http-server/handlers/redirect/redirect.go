package redirect_handler

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/Alzoww/url-shortner/pkg/logger/sl"
	"golang.org/x/exp/slog"
	"net/http"
)

type URLGetter interface {
	GetUrl(alias string) (string, error)
}

func New(log *slog.Logger, storage URLGetter) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		const f = "handlers.redirect.New"

		log = log.With(
			slog.String("operation", f),
			slog.String("request_id", middleware.GetReqID(r.Context())),
		)

		alias := chi.URLParam(r, "alias")

		if alias == "" {
			log.Info("alias is empty")
			http.Error(w, "alias is empty", http.StatusBadRequest)
			return
		}

		url, err := storage.GetUrl(alias)
		if err != nil {
			log.Error("failed to get url", sl.Err(err))
			http.Error(w, "failed to get url", http.StatusInternalServerError)
			return
		}

		log.Info("successfully got url", slog.String("url", url))

		// redirect to found url
		http.Redirect(w, r, url, http.StatusFound)
	}
}
