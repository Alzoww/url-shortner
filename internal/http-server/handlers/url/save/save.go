package save_handler

import (
	"encoding/json"
	"errors"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/Alzoww/url-shortner/pkg/logger/sl"
	"gitlab.com/Alzoww/url-shortner/pkg/random"
	"golang.org/x/exp/slog"
	"net/http"
)

type Request struct {
	URL   string `json:"url"`
	Alias string `json:"alias,omitempty"`
}

type Response struct {
	Status string `json:"status"`
	Error  string `json:"error,omitempty"`
	Alias  string `json:"alias,omitempty"`
}

type URLSaver interface {
	SaveUrl(urlToSave, alias string) (int64, error)
}

const aliasLength = 6

func New(log *slog.Logger, storage URLSaver) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		const f = "handlers.url.save.New"

		log = log.With(
			slog.String("operation", f),
			slog.String("request_id", middleware.GetReqID(r.Context())),
		)

		var req Request

		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			log.Error("error decoding request body", sl.Err(err))
			http.Error(w, "invalid request body", http.StatusBadRequest)
			return
		}

		err = validate(req)
		if err != nil {
			log.Error("error validating request: missing url", sl.Err(err))
			http.Error(w, "invalid request body: missing url", http.StatusBadRequest)
			return
		}

		log.Info("request body decoded successfully", slog.Any("request", req))

		alias := req.Alias
		if alias == "" {
			alias = random.NewRandomString(aliasLength)
		}

		id, err := storage.SaveUrl(req.URL, alias)
		if err != nil {
			log.Error("failed to add url", sl.Err(err))
			http.Error(w, "failed to add url", http.StatusInternalServerError)
			return
		}

		log.Info("url added", slog.Int64("id", id))

		resp := Response{
			Status: "success",
			Alias:  alias,
		}

		w.Header().Set("content-type", "application/json")

		err = json.NewEncoder(w).Encode(resp)
		if err != nil {
			log.Error("failed to encode response", sl.Err(err))
			http.Error(w, "failed to encode response", http.StatusInternalServerError)
			return
		}
	}
}

func validate(req Request) error {
	if req.URL == "" {
		return errors.New("missing url")
	}

	return nil
}
