package save_handler

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Alzoww/url-shortner/internal/http-server/handlers/url/save/mocks"
	slogdiscard "gitlab.com/Alzoww/url-shortner/pkg/logger/handlers/slogdiscaard"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestNewSuccess(t *testing.T) {
	request := Request{
		URL:   "http://example.com",
		Alias: "example.com",
	}

	log := slogdiscard.NewDiscardLogger()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	storage := mock_save_handler.NewMockStorager(ctrl)
	storage.EXPECT().SaveUrl(request.URL, request.Alias).Return(int64(1), nil).Times(1)

	h := New(log, storage)
	serverFunc := h.ServeHTTP

	rec := httptest.NewRecorder()

	testReq := httptest.NewRequest("POST", "/url", bytes.NewBuffer([]byte(
		`{
			"url": "http://example.com",
			"alias": "example.com"
		}`,
	)))
	testReq.Header.Set("Content-Type", "application/json")

	serverFunc(rec, testReq)

	res := rec.Result()
	defer res.Body.Close()

	data, err := io.ReadAll(res.Body)
	assert.NoError(t, err)
	expData := "{\"status\":\"success\",\"alias\":\"example.com\"}\n"

	assert.Equal(t, http.StatusOK, res.StatusCode)
	assert.Equal(t, expData, string(data))
}

func TestNewEmptyRequestUrl(t *testing.T) {
	log := slogdiscard.NewDiscardLogger()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	storage := mock_save_handler.NewMockStorager(ctrl)

	h := New(log, storage)
	serverFunc := h.ServeHTTP

	rec := httptest.NewRecorder()

	testReq := httptest.NewRequest("POST", "/url", bytes.NewBuffer([]byte(
		`{
			"url": "",
			"alias": "example.com"
		}`,
	)))
	testReq.Header.Set("Content-Type", "application/json")

	serverFunc(rec, testReq)

	res := rec.Result()
	defer res.Body.Close()

	data, err := io.ReadAll(res.Body)
	assert.NoError(t, err)
	expData := "invalid request body: missing url\n"

	assert.Equal(t, http.StatusBadRequest, res.StatusCode)
	assert.Equal(t, expData, string(data))
}

func TestNewEmptyAlias(t *testing.T) {
	request := Request{
		URL:   "http://example.com",
		Alias: "",
	}

	log := slogdiscard.NewDiscardLogger()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	storage := mock_save_handler.NewMockStorager(ctrl)
	storage.EXPECT().SaveUrl(request.URL, gomock.Any()).Return(int64(1), nil).Times(1)

	h := New(log, storage)
	serverFunc := h.ServeHTTP

	rec := httptest.NewRecorder()

	testReq := httptest.NewRequest("POST", "/url", bytes.NewBuffer([]byte(
		`{
			"url": "http://example.com",
			"alias": ""
		}`,
	)))
	testReq.Header.Set("Content-Type", "application/json")

	serverFunc(rec, testReq)

	res := rec.Result()
	defer res.Body.Close()

	data, err := io.ReadAll(res.Body)
	assert.NoError(t, err)
	var resp Response
	err = json.Unmarshal(data, &resp)
	assert.NoError(t, err)

	expData := fmt.Sprintf("{\"status\":\"success\",\"alias\":\"%s\"}\n", resp.Alias)

	assert.Equal(t, http.StatusOK, res.StatusCode)
	assert.Equal(t, expData, string(data))
}

func TestNewErrFromDB(t *testing.T) {
	request := Request{
		URL:   "http://example.com",
		Alias: "example.com",
	}

	log := slogdiscard.NewDiscardLogger()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	storage := mock_save_handler.NewMockStorager(ctrl)
	storage.EXPECT().SaveUrl(request.URL, request.Alias).Return(int64(0), errors.New("db is down")).Times(1)

	h := New(log, storage)
	serverFunc := h.ServeHTTP

	rec := httptest.NewRecorder()

	testReq := httptest.NewRequest("POST", "/url", bytes.NewBuffer([]byte(
		`{
			"url": "http://example.com",
			"alias": "example.com"
		}`,
	)))
	testReq.Header.Set("Content-Type", "application/json")

	serverFunc(rec, testReq)

	res := rec.Result()
	defer res.Body.Close()

	data, err := io.ReadAll(res.Body)
	assert.NoError(t, err)
	expData := "failed to add url\n"

	assert.Equal(t, http.StatusInternalServerError, res.StatusCode)
	assert.Equal(t, expData, string(data))
}

func Test_validate(t *testing.T) {

	tests := []struct {
		name    string
		req     Request
		wantErr bool
	}{
		{
			"valid req url", Request{URL: "https://example.com", Alias: "example"}, false,
		},
		{
			"empty req url", Request{URL: "", Alias: "example"}, true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := validate(tt.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
