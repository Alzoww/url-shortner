package handler

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/Alzoww/url-shortner/internal/config"
	redirect_handler "gitlab.com/Alzoww/url-shortner/internal/http-server/handlers/redirect"
	save_handler "gitlab.com/Alzoww/url-shortner/internal/http-server/handlers/url/save"
	mwLogger "gitlab.com/Alzoww/url-shortner/internal/http-server/middleware/logger"
	"gitlab.com/Alzoww/url-shortner/internal/storage/sqlite"
	"gitlab.com/Alzoww/url-shortner/pkg/logger/sl"
	"golang.org/x/exp/slog"
)

func Router(log *slog.Logger, cfg *config.Config) (*chi.Mux, error) {
	r := chi.NewRouter()

	r.Use(middleware.RequestID)
	//r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(mwLogger.New(log))
	r.Use(middleware.Recoverer)
	r.Use(middleware.URLFormat)

	storage, err := sqlite.New(cfg.StoragePath)
	if err != nil {
		log.Error("failed to init storage", sl.Err(err))
		return nil, err
	}

	// Basic auth
	r.Route("/url", func(router chi.Router) {
		router.Use(middleware.BasicAuth("url-shortner", map[string]string{
			cfg.HTTPServer.Username: cfg.HTTPServer.Password,
		}))

		router.Post("/", save_handler.New(log, storage))
		// TODO: add delete
	})

	r.Get("/{alias}", redirect_handler.New(log, storage))

	return r, nil
}
